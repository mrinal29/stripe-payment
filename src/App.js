import "@stripe/stripe-js";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Checkout from "./Component/Checkout";
import Success from "./Component/Success";
import Cancel from "./Component/Cancel";

import "./styles.css";

export default function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path='/' component={Checkout} />
          <Route exact path="/success" component={Success} />
          <Route exact path="/cancel" component={Cancel} />
         </Switch>
      </Router>
    </div>
  );
}


